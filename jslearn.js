// data types
console.log("data types -----------");

console.log("undefined =  " + typeof undefined);

console.log(" 53 = " + typeof 53);

console.log(" 100n = " + typeof 100n);

console.log(" true = " + typeof true);

console.log(" \"some string\" = " + typeof "some string");

console.log(' Symbol("id") = ' + typeof Symbol("id"));

console.log(" Math = " + typeof Math);

console.log("null = " + typeof null);

console.log(" alert() " + typeof alert);

// string quotes
console.log("string quotes -----------------");

let name = "John";
 
console.log(` hello ${1} `);

console.log(` hello ${name}`);

console.log(` hello ${"name"}`);

//type conversions
console.log("type conversions -------------");

let value = true;

value = String(value);

console.log(value + " -> " + typeof value);

console.log( "6" / '2');

let num_str = "123";

num_str = Number(num_str);

console.log(num_str + " -> " + typeof num_str);

num_str = Number("an arbitary string instead of a number");
console.log(num_str);

// prototypal inheritance

console.log("prototypal inheritance ----------- ");

let animal = {
    eats : true,
    walk() {
        console.log("Animal walk");
    }
};

let rabbit = { 
    jumps : true,
    __proto__: animal
};

let longEar = {
    earLength: 10,
    __proto__: rabbit
}

longEar.walk();

console.log(longEar.eats);

//working with prototype

console.log("working with prototype ----------");

animal = {
    jumps : null
};

rabbit = {
    __proto__: animal,
    jumps : true
};

console.log("1.rabbit.jumps : " + rabbit.jumps);

delete rabbit.jumps;

console.log("2.rabbit.jumps : " + rabbit.jumps);

delete animal.jumps;

console.log("3.rabbit.jumps : " + rabbit.jumps);


let user = {
    name: "John",
    surname: "Smith",

    set fullName(value) {
        [this.name, this.surname] = value.split(' ')
    },

    get fullName() {
        return `${this.name} ${this.surname}`;
    }
}

let admin = {
    __proto__: user,
    isAdmin: true
}

console.log(admin.fullName);

admin.fullName = "Alica Kyper";
console.log(admin.name);
console.log(admin.surname);

